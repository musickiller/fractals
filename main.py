#!/usr/local/bin/python3
import numpy as np
import pygame
from pygame import gfxdraw

h,w=480,640
border=50
N=0

class Stem:
    def __init__(self, surface, x=0, y=0, alpha=0, speed=1, max_length=0, color=(255,255,255), idn='0', border_x=0, border_y=0):
        self.surface = surface
        self.x = x
        self.y = y
        self.alpha = alpha
        self.speed = speed
        self.max_length = max_length
        self.length = 0
        self.children = []
        self.alive = True
        self.can_move = True
        self.color = color
        self.idn = idn
        self.border_x = border_x
        self.border_y = border_y
        # print(f"idn = {idn}")
        #print(f"max_length = {max_length}")

    def draw(self, color=(255,255,255), drawlimit=True, bg=(0,0,0)):
        surface = self.surface
        x = int(self.x)
        y = int(self.y)
        if drawlimit:
            z = tuple(surface.get_at((x,y)))[:-1]
            if not z == bg:
                return False
                
        gfxdraw.pixel(surface, x, y, color)
        return True

    def move(self):
        if self.max_length == 0 or self.can_move:
            self.length += self.speed
            if self.length > self.max_length:
                self.can_move = False
                return False
            else:
                x = self.x
                y = self.y
                alpha = self.alpha
                #print(f"position: {(x, y)}")
                xd = np.sin(np.deg2rad(alpha))
                yd = np.cos(np.deg2rad(alpha))
                #print(f"movement: {(xd, yd)}")
                self.x = x+xd
                self.y = y+yd
                return True


    def rotate(self, beta):
        alpha = self.alpha + beta
        if alpha <= -360:
            alpha = alpha % - 360
        elif alpha >= 360:
            alpha = alpha % 360
        
        self.alpha = alpha
        
class TreeSimple(Stem):
    def __init__(self, *args, **kwargs):
        """
        Constructor with some extra params:

        * verbose: be verbose about what we do. Defaults to True.

        For other params see: zipfile.ZipFile
        """
        self.shorting_factor = kwargs.pop('shorting_factor', 2)
        super().__init__(*args, **kwargs)

    def action(self):
        # print(f"alive = {self.alive}")
        color = self.color
        if not self.alive:
            return False
        elif self.x < 0\
            or self.y <0\
            or self.x > self.border_x\
            or self.y> self.border_y:
            self.alive = False
            return False
        elif self.length < self.max_length:
            # move while can
            self.move()
            self.draw(color=color)
            return True
        elif self.max_length <= 1:
            # can't move and too small - die
            self.alive = False
            return True
        elif len(self.children) == 0 and self.max_length > 1:
            # can't move, not small - give birth
            # since children are not deleted, it's not required to have other logic here
            # but it would be good to cleanup dead objects.
            self.spawn()
            return True
        else:
            # life goes on while children are alive
            children_alive = False
            for actor in self.children:
                alive = actor.action()
                if alive:
                    children_alive = True
            if not children_alive:
                self.alive = False
            return True

    def spawn(self):
        idn = 0
        selfidn = self.idn
        self.children.append(
            self.__class__(
                self.surface,
                self.x,
                self.y,
                self.alpha+45,
                max_length=self.max_length/self.shorting_factor,
                shorting_factor=self.shorting_factor,
                idn=selfidn+str(idn),
                color=self.color,
                border_x=self.border_x,
                border_y=self.border_y
        ))
        idn = 1
        self.children.append(
            self.__class__(
                self.surface,
                self.x,
                self.y,
                self.alpha-45,
                max_length=self.max_length/self.shorting_factor,
                shorting_factor=self.shorting_factor,
                idn=selfidn+str(idn),
                color=self.color,
                border_x=self.border_x,
                border_y=self.border_y
        ))


class ColorfulTree(TreeSimple):
    def __init__(self, *args, **kwargs):
        """
        Constructor with some extra params:

        * verbose: be verbose about what we do. Defaults to True.

        For other params see: zipfile.ZipFile
        """
        self.color_change_type = kwargs.pop('color_change_type', 'default')
        super().__init__(*args, **kwargs)

    def action(self):
        alive = super().action()
        if alive:
            self.change_color()
        return alive

    def change_color(self):
        if self.color_change_type == "default":
            self.change_color_default()

    def change_color_default(self):
        r, g, b = self.color
        if r == 255 and g < 255 and b == 0:
            g += 1
        elif r > 0 and g == 255 and b == 0:
            r -= 1
        elif r == 0 and g == 255 and b < 255:
            b += 1
        elif r == 0 and g > 0  and b == 255:
            g -= 1
        elif r < 255 and g == 0 and b == 255:
            r += 1
        elif r == 255 and g == 0 and b > 0:
            b -= 1
        else:
            raise Exception(f'Unknown color situation! {r, g, b}')
        self.color = (r,g,b)



pygame.init()
screen = pygame.display.set_mode((w+(2*border), h+(2*border)))
pygame.display.set_caption("Serious Work - not games")
done = False
clock = pygame.time.Clock()
surface = pygame.Surface((h,w))
y = h-border
x = int(w/2)
actor = ColorfulTree(surface, x, y, 180, max_length=20, shorting_factor=1.01, color=(255,255,0), border_x=h-border, border_y=w-border)
# actor = TreeSimple(surface, x, y, 180, max_length=100, shorting_factor=1.5)
alive = True

# Get a font for rendering the frame number
basicfont = pygame.font.SysFont(None, 32)

while not done:
        for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True

        if alive:
            alive = actor.action()
        

        # Convert to a surface and splat onto screen offset by border width and height
        
        screen.blit(surface, (border, border))

        # Display and update frame counter
        text = basicfont.render('Frame: ' + str(N), True, (255, 0, 0), (255, 255, 255))
        screen.blit(text, (border,h+border))
        N = N + 1

        pygame.display.flip()
        clock.tick(60)